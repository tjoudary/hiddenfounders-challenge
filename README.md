<p align="center"><img src="https://hiddenfounders.com/images/logo.svg"></p>

## About Web Coding Challenge

This project has been part of the Web Coding Challenge by the HiddenFounders. The premise of which is developing a Web App that does the following:
- Allow the user to login or register
- Display the list of shops sorted by distance
- Allow the user to like a shop for it to be moved to "My Preferred Shops"
- Allow the user to dislike a shop to hide it for 2 hours
- Allow the user to remove a shop from his preferred shops

This challenge has been taken up using PHP 7 with the help of the Laravel Framework.

## UML diagrams

In order to better map out the functions to be developed, we have designed a use-case diagram as well as a classes diagram.

**Use case diagram**
<p align="center"><img src="https://i.imgur.com/WzytSdV.jpg"></p>

**Class diagram**
<p align="center"><img src="https://i.imgur.com/pgfbn5g.jpg"></p>

## Notes

- We used a simple way to model the distance between the user and the shop, which is to give both of them X and Y coordinates. This allows us to calculate the distance using this simple formula: Sqrt(dX² - dY²). Where as dX = shopX - userX and dY = shopY - userY.
- The database is included in the files as wcc.sql. It contains 5 shops by default.

## Links

These are links to learn more about me

- **[Sourceforge](https://sourceforge.net/u/vbtheory/profile/)**
- **[Bitbucket](https://bitbucket.org/tjoudary/)**
- **[Youtube](https://youtube.com/vbtheory/)**
- **[LinkedIn](https://www.linkedin.com/in/mohamed-taha-joudary/)**

