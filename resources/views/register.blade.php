@extends('layouts.auth')

@section('content')
    <form method="POST" action="/register" class="form-signin">
        <input name="_token" type="hidden" value="{{csrf_token()}}">

        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" required="" autofocus=""><br>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" name="password" id="inputPassword" class="form-control" style="width: 48%; display: inline-block" placeholder="Password" required="">
        <input type="password" name="password_confirmation" id="repeatPassword" class="form-control" style="width: 48%; display: inline-block" placeholder="Repeat password" required=""><br><br>
        <label for="inputLocation" class="sr-only">Location</label>
        <input type="number" name="X" id="inputX" class="form-control" style="width: 48%; display: inline-block" placeholder="X coordinate" required="">
        <input type="number" name="Y" id="inputY" class="form-control" style="width: 48%; display: inline-block" placeholder="Y coordinate" required=""><br><br>

        <button class="btn btn-lg btn-primary btn-block" type="submit">Create account</button>

        <div class="text-center p-t-12">
            <a class="txt2" href="/login">
                Sign in
            </a>
        </div>

    </form>
@endsection