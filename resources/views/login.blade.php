@extends('layouts.auth')

@section('content')
    <form method="POST" action="/login" class="form-signin">
        <input name="_token" type="hidden" value="{{csrf_token()}}">

        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" required="" autofocus="">
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required=""><br>

        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>

        <div class="text-center p-t-12">
            <a class="txt2" href="/register">
                Create account
            </a>
        </div>

    </form>
@endsection