<div class="row">
    <div class="col-md-12">
        <div style="text-align: right">
            <ul class="list-inline">
                @if($selected == 0)
                    <li class="pull-right list-inline-item">Nearby Shops</li>
                    <li class="pull-right list-inline-item"><a href="/favorites">My Preferred Shops</a></li>
                @else
                    <li class="list-inline-item"><a href="/">Nearby Shops</a></li>
                    <li class="list-inline-item">My Preferred Shops</li>
                @endif
            </ul>
        </div>
    </div>
</div>