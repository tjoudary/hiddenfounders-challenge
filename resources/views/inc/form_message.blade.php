<script type="text/javascript">

    toastr.options = {
        "closeButton": true,
        "debug": false,
        "positionClass": "toast-bottom-right",
        "showDuration": "1000",
        "hideDuration": "200",
        "timeOut": "8000",
        "extendedTimeOut": "2000",
        "showEasing": "swing",
        "hideEasing": "swing",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

@if($errors->any())
    toastr.error('{{$errors->first()}}', 'Operation failed');
@endif

@if(session()->has('error'))
    toastr.error('{{session()->get('error')}}', 'Operation failed');
@endif

@if(session()->has('message'))
    toastr.success('{{ session()->get('message') }}', 'Success');
@endif

</script>
