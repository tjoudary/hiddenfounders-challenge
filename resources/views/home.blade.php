@extends('layouts.app')

@section('content')
<div class="container">

    @include('inc.menu', ['selected' => 0])

    <div class="row">
        @if(sizeof($shops) == 0)
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        No shops found nearby.
                    </div>

                </div>
            </div>
        @endif
        @foreach($shops as $shop)
            <div class="col-md-4 shop-element" data-shop-id="{{$shop->id}}">
                <div class="card">
                    <div class="card-header">{{$shop->name}}</div>

                    <div class="card-body">
                        <b>Location: </b>[ {{ $shop->position_x }}, {{ $shop->position_y }} ]
                        <br>
                        <b>Distance: </b>{{ round($shop->getDistanceFromUser($user), 2) }}
                    </div>

                    <div class="card-footer">
                        <a href="#" class="dislike_shop btn btn-danger" data-shop-id="{{$shop->id}}">Dislike</a>
                        <a href="#" class="like_shop btn btn-success" data-shop-id="{{$shop->id}}">Like</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
@endsection

@section('scripts')

    <script type="text/javascript">

        // Events for like & dislike buttons

        $(document).ready(function(e){
            $('.dislike_shop').click(function (e) {
                e.preventDefault();

                var shop_id = $(this).attr('data-shop-id');

                if(!confirm("Disliking this shop will hide it for two hours. Are you sure about this?")) return;
                $.ajax({
                    url: '/ajax/dislike',
                    cache: false,
                    type: 'GET',
                    data: 'shop_id=' + shop_id,
                    success: function (response) {
                        response = JSON.parse(response);

                        if(response.result) {
                            $('.shop-element[data-shop-id="'+shop_id+'"]').fadeOut(200);
                            toastr.success('', response.message);
                        } else {
                            toastr.error('', response.message);
                        }

                    }
                });
            });

            $('.like_shop').click(function (e) {
                e.preventDefault();

                var shop_id = $(this).attr('data-shop-id');

                $.ajax({
                    url: '/ajax/like',
                    cache: false,
                    type: 'GET',
                    data: 'shop_id=' + shop_id,
                    success: function (response) {
                        response = JSON.parse(response);

                        if(response.result) {
                            $('.shop-element[data-shop-id="'+shop_id+'"]').fadeOut(200);
                            toastr.success('', response.message);
                        } else {
                            toastr.error('', response.message);
                        }

                    }
                });
            });
        })


    </script>

@endsection
