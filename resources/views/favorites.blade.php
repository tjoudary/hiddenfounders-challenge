@extends('layouts.app')

@section('content')
<div class="container">

    @include('inc.menu', ['selected' => 1])

    <div class="row">
        @if(sizeof($shops) == 0)
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        No shops liked.
                    </div>

                </div>
            </div>
        @endif
        @foreach($shops as $shop)
            <div class="col-md-4 shop-element" data-shop-id="{{$shop->id}}">
                <div class="card">
                    <div class="card-header">{{$shop->name}}</div>

                    <div class="card-body">
                        <b>Location: </b>[ {{ $shop->position_x }}, {{ $shop->position_y }} ]
                        <br>
                        <b>Distance: </b>{{ round($shop->getDistanceFromUser($user), 2) }}
                    </div>

                    <div class="card-footer">
                        <a href="#" data-shop-id="{{$shop->id}}" class="remove_shop btn btn-danger">Remove</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
@endsection

@section('scripts')

    <script type="text/javascript">

        // Events for the remove button

        $(document).ready(function(e){
            $('.remove_shop').click(function (e) {
                e.preventDefault();

                var shop_id = $(this).attr('data-shop-id');

                $.ajax({
                    url: '/ajax/remove',
                    cache: false,
                    type: 'GET',
                    data: 'shop_id=' + shop_id,
                    success: function (response) {
                        response = JSON.parse(response);

                        if(response.result) {
                            $('.shop-element[data-shop-id="'+shop_id+'"]').fadeOut(200);
                            toastr.success('', response.message);
                        } else {
                            toastr.error('', response.message);
                        }
                    }
                });
            });

        })


    </script>

@endsection
