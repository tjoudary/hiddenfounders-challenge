<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'position_x', 'position_y'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Finds the list of shops nearby (minus disliked shops) ordered by distance.
     *
     * @return array
     */
    public function getNearbyShops(){
        // Gets all the shops
        $shops = Shop::all()->sortBy(function($shop)
        {
            return $shop->getDistanceFromUser($this);
        });

        $result = [];

        foreach ($shops as $shop){
            // Remove all expired dislikes
            PreferenceList::where('created_at', '<', Carbon::now()->subHours(2)->toDateTimeString())
                ->where('liked', 0)->delete();

            // Filters out the shops that have been either liked or disliked
            if(PreferenceList::where('shop_id', $shop->id)->where('user_id', $this->id)->count())
                continue;

            // Add shop to results
            array_push($result, $shop);
        }

        return $result;
    }

    /**
     * Finds the list of favorite shops.
     *
     * @return array
     */
    public function getLikedShops(){
        // Gets all the shops
        $shops = Shop::all();
        $user_id = Auth::user()->id;

        $result = [];

        foreach ($shops as $shop){
            // Add shop to results if it's liked
            if($shop->getPreferredState($user_id) === 1) array_push($result, $shop);
        }

        return $result;
    }
}
