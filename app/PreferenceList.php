<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PreferenceList extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'shop_id', 'liked'
    ];

}
