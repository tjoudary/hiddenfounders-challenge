<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'position_x', 'position_y'
    ];

    /**
     * Finds the preferred state of this shop for the selected user.
     *
     * @param $user_id
     * @return null = no preference
     *         0    = disliked
     *         1    = liked
     */
    function getPreferredState($user_id){
        // Get preference state
        $preference = PreferenceList::where('user_id', $user_id)->where('shop_id', $this->id)->first();

        // Returns either null or the liked state
        return optional($preference)->liked;
    }

    /**
     * Finds the distance from this shop to the selected user.
     *
     * @param $user
     * @return null = no preference
     *         0    = disliked
     *         1    = liked
     */
    function getDistanceFromUser($user){
        // The formula is: Sqrt( (shop_x - user_x)² + (shop_y - user_y)² )
        $delta_x = $this->position_x - $user->position_x;
        $delta_y = $this->position_y - $user->position_y;

        return sqrt($delta_x * $delta_x + $delta_y * $delta_y);
    }


}
