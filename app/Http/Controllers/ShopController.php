<?php

namespace App\Http\Controllers;

use App\PreferenceList;
use App\Shop;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ShopController extends Controller
{

    /**
     * Ajax method that likes the selected shop for the logged in user.
     *
     * @param Request $request
     * @return array
     */
    public function likeShop(Request $request)
    {
        $result = $this->changeShopPreference($request->shop_id, true);

        return json_encode(
            [
                'message' => $result?'Shop liked':'Coudln\'t find the shop',
                'result' => $result
            ]
        );
    }

    /**
     * Ajax method that dislikes the selected shop for the logged in user.
     *
     * @param Request $request
     * @return array
     */
    public function dislikeShop(Request $request)
    {
        $result = $this->changeShopPreference($request->shop_id, false);

        return json_encode(
            [
                'message' => $result?'Shop disliked':'Coudln\'t find the shop',
                'result' => $result
            ]
        );
    }

    /**
     * Ajax method that removes like on the selected shop for the logged in user.
     *
     * @param Request $request
     * @return array
     */
    public function removeLike(Request $request)
    {
        $result = $this->changeShopPreference($request->shop_id, null);

        return json_encode(
            [
                'message' => $result?'Like removed':'Coudln\'t find the shop',
                'result' => $result
            ]
        );
    }

    /**
     * Method that changes the shop preference for the logged in user.
     *
     * @param $shop_id
     * @param $liked
     * @return bool
     *
     */
    public function changeShopPreference($shop_id, $liked)
    {
        // Finds the nearby shops of the logged in user
        $user = Auth::user();
        $shop = Shop::find($shop_id);

        // If shop not found, return false
        if(!$shop) return false;

        // Removes previous preference
        PreferenceList::where('user_id', $user->id)->where('shop_id', $shop->id)->delete();

        if($liked !== null){
            // Saved the new preference
            PreferenceList::create([
                'user_id' => $user->id,
                'shop_id' => $shop->id,
                'liked' => $liked
            ]);
        }

        return true;
    }

}
