<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the list of nearby shops.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Finds the nearby shops of the logged in user
        $user = Auth::user();
        $shops = $user->getNearbyShops();

        return view('home')
            ->with('shops', $shops)
            ->with('user', $user);
    }

    /**
     * Show the favorite shops list.
     *
     * @return \Illuminate\Http\Response
     */
    public function favorites()
    {
        // Gets parameters to be sent to the favorites view
        $user = Auth::user();
        $shops = $user->getLikedShops();

        return view('favorites')
            ->with('shops', $shops)
            ->with('user', $user);
    }

}
