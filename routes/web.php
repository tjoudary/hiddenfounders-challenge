<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/favorites', 'HomeController@favorites')->name('favorites');

Route::get('/ajax/like', 'ShopController@likeShop')->name('shop.like');
Route::get('/ajax/dislike', 'ShopController@dislikeShop')->name('shop.dislike');
Route::get('/ajax/remove', 'ShopController@removeLike')->name('shop.removeLike');

Route::get('/login', 'Auth\LoginController@login')->name('login');
Route::get('/register', 'Auth\RegisterController@register')->name('register');

Route::post('/login', 'Auth\LoginController@loginUser')->name('login.form');
Route::post('/register', 'Auth\RegisterController@registerUser')->name('register.form');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');
